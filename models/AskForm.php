<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class AskForm extends Model
{
    public $name;
    public $phone;
    public $email;
    public $body;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['name', 'required', 'message' => 'Пожалуйста, введите Ваше имя'],
            ['phone', 'required', 'message' => 'Пожалуйста, введите Ваш номер телефона'],
            ['email', 'required', 'message' => 'Пожалуйста, введите Ваш email'],
            ['body', 'required', 'message' => 'Пожалуйста, введите текст сообщения'],
            ['email', 'email', 'message' => 'введеное значение не является email адресом'],
            //Ориентировано на российские мобильные + городские с кодом из 3 цифр (например, Москва).
            ['phone', 'match', 'pattern' => '/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/', 'message' => 'телефон введен некорректно']
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => '',
            'phone' => '',
            'email' => '',
            'body' => '',
        ];
    }

    public function MergeBody($name, $phone, $body)
    {
        return $name.'  '.$phone."\r\n".$body;;
    }
    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param  string  $email the target email address
     * @return boolean whether the model passes validation
     */
    public function contact($email)
    {
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom([$this->email => $this->name])
                ->setTextBody($this->MergeBody($this->name, $this->phone, $this->body))
                ->send();

            return true;
        } else {
            return false;
        }
    }
}

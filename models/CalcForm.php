<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class CalcForm extends Model
{
    // public $name;
    // public $phone;
    // public $email;
    public $org_name;
    public $client_name;
    public $client_phone;
    public $client_requisites;

    public $number_comps;
    public $number_servers;
    public $number_printers;
    public $number_offices;

    // public $remote_Administration = ['value' => '200'];
    public $remote_Administration;

    public $tarifs = [0 =>'Silver',1 => 'Gold',2 => 'Platinum', 3 => 'Индивидуальный'];

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['org_name', 'required', 'message' => 'Пожалуйста, введите название Вашей организации'],
            ['client_name', 'required', 'message' => 'Пожалуйста, введите Ваше имя'],
            ['client_phone', 'required', 'message' => 'Пожалуйста, введите Ваш телефон'],
            // //Ориентировано на российские мобильные + городские с кодом из 3 цифр (например, Москва).
            ['client_phone', 'match', 'pattern' => '/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/', 'message' => 'телефон введен некорректно'],

            ['number_comps', 'default', 'value' => 5],
            ['number_comps', 'required', 'message' => 'Пожалуйста, введите количество компьютеров'],
            ['number_comps', 'integer', 'min' => 1],

            ['number_comps', 'default', 'value' => 1],
            ['number_offices', 'required', 'message' => 'Пожалуйста, введите количество офисов'],
            ['number_comps', 'integer', 'min' => 1],



            ['remote_Administration', 'boolean', 'message' => ''],





        ];
    }

    public function attributeLabels()
    {
        return [
            'org_name' => '',
            'client_name' => '',
            'client_requisites' => '',
            'client_phone' => '',
            'number_comps' => 'Количество компьютеров',
            'number_servers' => 'Количество серверов',
            'number_printers' => 'Количество принтеров',
            'number_offices' => 'Количество офисов',
            'tarifs' => 'тарифы',


            'remote_Administration' => '',


            'client_requisites' => '',
            'client_phone' => '',
            'client_phone' => '',
            'client_phone' => '',
            'client_phone' => '',
        ];
    }
    public function getAttributeValue($attribute)
    {
        $AttributeValue = [
            'remote_Administration' => 200
        ];

        if(isset($AttributeValue[$attribute])){
            return $AttributeValue[$attribute];
        }else{
            return 0;
        }

    }
    public function MergeBody()
    {
        $body = '';
        $remote_Administration = "Удаленное администрирование";
        $body .= $this->org_name;
        $body .= $this->client_name;
        $body .= $this->client_phone;

        if ($this->remote_Administration) {
            $body .= $remote_Administration;
        }
        return $body;
    }
    public function calc($email)
    {
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo($email)
                // ->setFrom([$this->email => $this->name])
                // ->setSubject($this->subject)
                ->setHtmlBody($this->MergeBody())
                ->send();

            return true;
        } else {
            return false;
        }
    }




}


 // public static function activeCheckbox($model, $attribute, $options = [])
 //    {
 //        $name = isset($options['name']) ? $options['name'] : static::getInputName($model, $attribute);
 //        $value = static::getAttributeValue($model, $attribute);

 //        if (!array_key_exists('value', $options)) {
 //            $options['value'] = '1';
 //        }
 //        if (!array_key_exists('uncheck', $options)) {
 //            $options['uncheck'] = '0';
 //        }

 //        $checked = "$value" === "{$options['value']}";

 //        if (!array_key_exists('id', $options)) {
 //            $options['id'] = static::getInputId($model, $attribute);
 //        }

 //        return static::checkbox($name, $checked, $options);
 //    }

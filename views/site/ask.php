<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
?>

<div class="wrapper ma">
        <header>
            <div class="cover">
                <div class="cover-bg">
                    <div class="cover-in"><div class="puska"></div>
                        <div class="container">
                            <a href='/site/index' class="logo"><img src="/images/logo.png" height="130" width="258"></a>
                            <div class="menu">
                                <ul class="menu_list">
                                    <li><a href='/site/index'>О компании</a></li>
                                    <li><a href="#">Информация</a>
                                        <ul>
                                            <li><a href='/site/calc'>Цены и тарифы</a></li>
                                            <li><a href='/site/about'>Порядок выполнения работ</a></li>
                                            <li><a href='/site/about'>Гарантии</a></li>
                                            <li><a href='/site/partner'>Партнерство</a></li>
                                        </ul>
                                    </li>
                                    <li class="active"><a href='/site/ask'>ЗаДАТЬ ВОПРОС</a></li>
                                    <li><a href='/site/contact'>Контакты</a></li>
                                </ul></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clears"></div>
        </header>





    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>


        <div class="container">
           <div class="bodz">

           Спасибо, что связались с нами, мы ответим в ближайшее время.

               <div class="anti_foot"></div>
           </div><!--bodz-->
        </div>

         <div class="clears"></div>


    <?php else: ?>
        <div class="container">
           <div class="bodz">
             <h1>ЗАДАТЬ ВОПРОС</h1>
<!--               <form action="">
 -->                 <title>Форма обратной связи</title>

            <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
                <?= $form->field($model, 'name')->textInput(['placeholder' => 'Имя', 'class' => 'asks'])?>
                <?= $form->field($model, 'email')->textInput(['placeholder' => 'Email', 'class' => 'asks'])?>
                <?= $form->field($model, 'phone')->textInput(['placeholder' => 'Телефон', 'class' => 'asks'])?>
                <?= $form->field($model, 'body')->textArea(['rows' => 6, 'placeholder' => 'Тексt сообщения', 'class' => 'asks'])?>
                <div class="form-group">
                    <?= Html::submitButton('ОТПРАВИТЬ', ['class' => 'asks_btn', 'name' => 'contact-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>


               <div class="anti_foot"></div>
           </div><!--bodz-->
        </div>

         <div class="clears"></div>
         <?php endif; ?>

<footer>
  <div class="fcontact clear-fix">
    <a href="callto:84951140450" class=ftel>8(495)11-404-50</a><br>
    <a href="mailto:support@xmemo.ru" class="fmail"><span>E-mail:</span> support@xmemo.ru</a>
    <p>xMemo © 2009-2014</p>
  </div>
    <a class="pushka" href="http://pushca.ru">Создание и поддержка сайтов<br>PUSH creative agency</a>
</footer>

<!-- S.M. -->

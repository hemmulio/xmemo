<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\CalcForm;
?>
<div class="wrapper">
        <header>
            <div class="cover">
                <div class="cover-bg">
                    <div class="cover-in"><div class="puska"></div>
                        <div class="container">
                            <a href='/site/index' class="logo"><img src="/images/logo.png" height="130" width="258"></a>
                            <div class="menu">
                                <ul class="menu_list">
                                    <li><a href='/site/index'>О компании</a></li>
                                    <li class="active"><a href="#">Информация</a>
                                        <ul>
                                            <li><a href='/site/calc'>Цены и тарифы</a></li>
                                            <li><a href='/site/about'>Порядок выполнения работ</a></li>
                                            <li><a href='/site/about'>Гарантии</a></li>
                                            <li><a href='/site/partner'>Партнерство</a></li>
                                        </ul>
                                    </li>
                                    <li><a href='/site/ask'>ЗаДАТЬ ВОПРОС</a></li>
                                    <li><a href='/site/contact'>Контакты</a></li>
                                </ul></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clears"></div>
        </header>



<div class="clears"></div>
<div class="container z">
  <?php $form = ActiveForm::begin(['id' => 'calc-form']); ?>
  <div class="cal">
    <h1>ЦЕНЫ И ТАРИФЫ</h1>
    <p>Сервис самостоятельного расчета стоимости обслуживания компьютерной инфраструктуры компании по всем действующим тарифам обслуживания компьютеров. Онлайн заявка на заключение договора обслуживания.</p>
  </div><!--cal-blog-->

  <div class="calc-blog">

    <div class="row">
      <div class="cell-row-1 img-calc"></div>

      <div class="cell-row-2">
        <div class="row-level-2">
          <div class="cell-level-2">
            <?= $form->field($model, 'number_comps')->textInput(['placeholder' => '5', 'class' => 'data'])?>
          </div>
          <div class="cell-level-2">
            <?= $form->field($model, 'number_printers')->textInput(['placeholder' => '5', 'class' => 'data'])?>
          </div>
          <div class="cell-level-2">
            <?= $form->field($model, 'tarifs[]')->dropdownlist($model->tarifs);?>
          </div>
        </div> <!-- row-level-2 -->

        <div class="row-level-2">
          <div class="cell-level-2">
            <?= $form->field($model, 'number_servers')->textInput(['placeholder' => '5', 'class' => 'data'])?>
          </div>
          <div class="cell-level-2">
            <?= $form->field($model, 'number_offices')->textInput(['placeholder' => '1', 'class' => 'data'])?>
          </div>
          <div class="cell-level-2 wleft">
            цена
          </div>
        </div> <!-- row-level-2 -->

    </div> <!-- cell-row-2 -->

    </div> <!-- row -->


  </div><!--calc-blog-->

  <div class="select-blog clear">
    <div class="add-blog">
      <div class="add-head">Базовые услуги</div>

      <div class="add-in">
        <div class="chose">
          <?=$form->field($model, 'remote_Administration')->checkbox();?>
        </div>
        <p>Удаленное администрирование</p>
        <div class="infa">
          <div class="descript">
            <div class="des-name">Подключение к вашему ПК через
              интернет для устранения ошибок
              и настройки ПО</div>
            <div class="des-des">Позволяет оперативно устранять
              проблемы с ПО без физического
              приезда к клиенту</div>
          </div><!--descript-->
        </div><!--infa-->
      </div><!--add-in-->
      <div class="clears"></div>


      <div class="add-in">
        <div class="chose">
         <input type="checkbox" value="200">
        </div>
        <p>Консультации по телефону</p>
        <div class="infa">
          <div class="descript">
            <div class="des-name">Возможность позвонить в службу поддержки в рабочее время по вопросам, оговоренным в договоре обслуживания.</div>
            <div class="des-des">Количество звонков не ограниченно</div>
          </div>
       </div>
      </div>
      <div class="clears"></div>


      <div class="add-in">
        <div class="chose">
         <input type="checkbox" value="200">
        </div>
        <p>Первоначальная настройка ПО </p>
        <div class="infa">
          <div class="descript">
            <div class="des-name">Настройка ПО сразу после заключения договора обслуживания для приведения ПО в рабочее состояние.</div>
            <div class="des-des">Далее проводится текущая поддержка ПО</div>
          </div>
        </div>
      </div>
      <div class="clears"></div>


      <div class="add-in">
        <div class="chose">
         <input type="checkbox" value="200">
        </div>
        <p>Установка ПО</p>
        <div class="infa">
          <div class="descript">
            <div class="des-name">Установка и настройка ПО согласно параметрам договора обслуживания.</div>
            <div class="des-des">Установка производится по запросу клиента или в случае явной необходимости</div>
          </div>
        </div>
      </div>
      <div class="clears"></div>


      <div class="add-in">
        <div class="chose">
         <input type="checkbox" value="200">
        </div>
        <p>Настройка локальной сети</p>
        <div class="infa">
          <div class="descript">
            <div class="des-name">Изменение настроек компьютеров и активного сетевого оборудования.</div>
            <div class="des-des">для корректной работы локальной сети при неисправностях или изменениях</div>
          </div>
        </div>
      </div>
      <div class="clears"></div>


      <div class="add-in">
        <div class="chose">
         <input type="checkbox" value="200">
        </div>
        <p>Неограниченное количество<br>аварийных выездов </p>
        <div class="infa">
          <div class="descript">
            <div class="des-name">При невозможности удаленного исправления ошибок с ИТ оборудованием клиента:</div>
            <div class="des-des">производится столько выездов к клиенту СКОЛЬКО ПОТРЕБУЕТСЯ для устранения ошибок</div>
          </div>
        </div>
      </div>
      <div class="clears"></div>


      <div class="add-in">
        <div class="chose">
         <input type="checkbox" value="200">
        </div>
        <p>2 профилактических выезда в месяц </p>
        <div class="infa">
          <div class="descript">
            <div class="des-name">При профилактическом выезде выполняется:</div>
            <div class="des-des">проверка состояния аппаратной и программной части оборудования, антивирусной защиты и резервного копирования</div>
          </div>
        </div>
      </div>
      <div class="clears"></div>


      <div class="add-in">
        <div class="chose">
         <input type="checkbox" value="200">
        </div>
        <p>Диагностика, ремонт, модернизация компьютеров (не входит стоимость комплектующих) </p>
        <div class="infa">
          <div class="descript">
            <div class="des-name">Выявление неисправностей и устранение неисправностей компьютеров:</div>
            <div class="des-des">Комплектующие для замены оплачиваются отдельно</div>
          </div>
        </div>
      </div>
      <div class="clears"></div>


      <div class="add-in">
        <div class="chose">
         <input type="checkbox" value="200">
        </div>
        <p>Время прибытия по аварийному <br>вызову  <span><input type="number" value="3"> </span>часа</p>
        <div class="infa">
          <div class="descript">
            <div class="des-name">Максимальное время прибытия к клиенту после получения информации о срочном вызове:</div>
            <div class="des-des">при невозможности решения задачи удаленно</div>
          </div>
        </div>
      </div>
      <div class="clears"></div>
    </div><!--add-blog. first colmn-->

    <div class="add-blog">
      <div class="add-head">Cредства связи</div>

      <div class="add-in">
        <div class="chose">
         <input type="checkbox" value="200">
        </div>
        <p>АТС</p>
        <div class="infa">
          <div class="descript">
            <div class="des-name">Текущая поддержка корректных настроек АТС:</div>
            <div class="des-des">и изменение настроек по запросу клиента</div>
          </div>
        </div>
      </div>
      <div class="clears"></div>


      <div class="add-in">
        <div class="chose">
         <input type="checkbox" value="200">
        </div>
        <p>IP телефония</p>
        <div class="infa">
          <div class="descript">
            <div class="des-name">Текущая поддержка корректных настроек IP телефонии:</div>
            <div class="des-des">и изменение настроек по запросу клиента</div>
          </div>
        </div>
      </div>
      <div class="clears"></div>


      <div class="add-in">
        <div class="chose">
         <input type="checkbox" value="200">
        </div>
        <p>Интернет</p>
        <div class="infa">
          <div class="descript">
            <div class="des-name">Поддержка корректных настроек ИТ оборудования:</div>
            <div class="des-des">для постоянного доступа пользователей к интернету</div>
          </div>
        </div>
      </div>
      <div class="clears"></div>


      <div class="add-in">
        <div class="chose">
         <input type="checkbox" value="200">
        </div>
        <p>Корпоративная почта</p>
        <div class="infa">
          <div class="descript">
            <div class="des-name">Настройка учетных записей электронной почты:</div>
            <div class="des-des">пользователей и настройка почтовой программы клиента в целях их корректной работы</div>
          </div>
        </div>
      </div>
      <div class="clears"></div>


      <div class="add-blog in">
        <div class="add-head">Cредства связи</div>

        <div class="add-in">
          <div class="chose">
            <input type="checkbox" value="200">
          </div>

          <p>Проводная сеть</p>
          <div class="infa">
            <div class="descript">
              <div class="des-name">Выполнение настроек и поддержка корректной работы:</div>
              <div class="des-des">активного и пассивного оборудования проводной локальной сети</div>
            </div>
          </div>
        </div>
        <div class="clears"></div>


        <div class="add-in">
          <div class="chose">
           <input type="checkbox" value="200">
          </div>
          <p>Бепроводная сеть</p>
          <div class="infa">
            <div class="descript">
                <div class="des-name">Выполнение настроек и поддержка корректной работы:</div>
              <div class="des-des">оборудования беспроводной локальной сети</div>
            </div>
          </div>
        </div>
        <div class="clears"></div>


        <div class="add-in">
          <div class="chose">
           <input type="checkbox" value="200">
          </div>
          <p>Гибридная сеть</p>
          <div class="infa">
            <div class="descript">
                <div class="des-name">Выполнение настроек и поддержка корректной работы:</div>
              <div class="des-des">активного и пассивного оборудования совмещенной проводной и беспроводной локальной сети</div>
            </div>
          </div>
        </div>
        <div class="clears"></div>


        <div class="add-in">
          <div class="chose">
           <input type="checkbox" value="200">
          </div>
          <p>Оборудование Cisco</p>
          <div class="infa">
            <div class="descript">
                <div class="des-name">Конфигурирование и настройка оборудования Cisco:</div>
              <div class="des-des">Интеграция оборудования Cisco в имеющуюся сетевую инфраструктуру</div>
            </div>
          </div>
        </div>
        <div class="clears"></div>


      </div><!--add-blog in-->
    </div><!--add-blog. sec colmn-->

    <div class="add-blog">
      <div class="add-head">Cерверы</div>

      <div class="add-in ">
        <div class="chose">
         <input type="checkbox" value="200">
        </div>
        <p>Шлюз интернет </p>
        <div class="infa">
          <div class="descript">
            <div class="des-name">Позволяет ограничить доступ к сайтам по заданным параметрам:</div>
            <div class="des-des">интернет трафик группы пользователей</div>
          </div>
        </div>
      </div>

      <div class="add-in ">
        <div class="chose">
         <input type="checkbox" value="200">
        </div>
        <p>Система учета трафика </p>
        <div class="infa">
          <div class="descript">
            <div class="des-name">Позволяет считать, а так же ограничивать по объему и скорости:</div>
            <div class="des-des">контролировать трафик, защитить от хакерских атак</div>
          </div>
        </div>
      </div>

      <div class="add-in ">
        <div class="chose">
         <input type="checkbox" value="200">
        </div>
        <p>Файловый сервер </p>
        <div class="infa">
          <div class="descript">
            <div class="des-name">Файловый сервер позволяет хранить данные:</div>
            <div class="des-des">предназначенные для общего доступа пользователей</div>
          </div>
        </div>
      </div>

      <div class="add-in ">
        <div class="chose">
         <input type="checkbox" value="200">
        </div>
        <p>Почтовый сервер </p>
        <div class="infa">
          <div class="descript">
            <div class="des-name">Поддержка корректной работы программы передачи и приема:</div>
            <div class="des-des">сообщений электронной почты</div>
          </div>
        </div>
      </div>

      <div class="add-in ">
        <div class="chose">
         <input type="checkbox" value="200">
        </div>
        <p>Почтовый сервер Exchange </p>
        <div class="infa">
          <div class="descript">
            <div class="des-name">Кроме приема и передачи электронной почты позволяет получить:</div>
            <div class="des-des">совместный доступ к календарям и задачам, поддержка мобильных устройств и веб доступ</div>
          </div>
        </div>
      </div>

      <div class="add-in ">
        <div class="chose">
         <input type="checkbox" value="200">
        </div>
        <p>Контроллер домена </p>
        <div class="infa">
          <div class="descript">
            <div class="des-name">Текущая поддержка сервера, контролирующего область локальной сети (домен):</div>
            <div class="des-des">который управляет взаимодействием пользователей и сервера</div>
          </div>
        </div>
      </div>

      <div class="add-in ">
        <div class="chose">
         <input type="checkbox" value="200">
        </div>
        <p>Терминальный сервер </p>
        <div class="infa">
          <div class="descript">
            <div class="des-name">Поддержка сервера, предоставляющего пользователям вычислительные ресурсы для решения задач:</div>
            <div class="des-des">Для работы пользователям необходим минимальный ПК для подключения к серверу</div>
          </div>
        </div>
      </div>

      <div class="add-in ">
        <div class="chose">
         <input type="checkbox" value="200">
        </div>
        <p>Терминальный сервер </p>
        <div class="infa">
          <div class="descript">
            <div class="des-name">Поддержка сервера, предоставляющего пользователям вычислительные ресурсы для решения задач:</div>
            <div class="des-des">Для работы пользователям необходим минимальный ПК для подключения к серверу</div>
          </div>
        </div>
      </div>

      <div class="add-in ">
        <div class="chose">
         <input type="checkbox" value="200">
        </div>
        <p>VPN сервер </p>
        <div class="infa">
          <div class="descript">
            <div class="des-name">Поддержка сервера, позволяющего обеспечить защищенное соединение нескольких локальных сетей:</div>
            <div class="des-des">Например, главный офис - филиал</div>
          </div>
        </div>
      </div>

      <div class="add-in ">
        <div class="chose">
         <input type="checkbox" value="200">
        </div>
        <p>Сервер приложений </p>
        <div class="infa">
          <div class="descript">
            <div class="des-name">Поддержка сервера, который предназначен для эффективного исполнения:</div>
            <div class="des-des">процедур, программ, механических операций, скриптов, которые поддерживают построение приложений</div>
          </div>
        </div>
      </div>

      <div class="add-in ">
        <div class="chose">
         <input type="checkbox" value="200">
        </div>
        <p>Сервер баз данных </p>
        <div class="infa">
          <div class="descript">
            <div class="des-name">Поддержка сервера, который обслуживает базу данных и отвечает за целостность и сохранность данных:</div>
            <div class="des-des">а также обеспечивает операции ввода-вывода при доступе клиента к информации</div>
          </div>
        </div>
      </div>

      <div class="add-in">
        <p>
          Для того чтобы выбрать услуги этого
          раздела, необходимо указать
          количество серверов в вашей
          организации.
        </p>
      </div>
    </div><!--add-blog three colmn-->

    <div class="clears"></div>

    <div class="add-serv">
      <div class="add-head">Дополнительные услуги</div>

      <div class="blogz">

        <div class="add-in ">
            <div class="chose">
             <input type="checkbox" value="200">
            </div>
            <p>Диагностика, ремонт, модернизация компьютеров (стоимость комплектующих входит в стоимость) </p>
            <div class="infa">
              <div class="descript">
                <div class="des-name">Выявление неисправностей и устранение неисправностей компьютеров:</div>
                <div class="des-des">БЕЗ ОПЛАТЫ замененных комплектующих</div>
              </div>
            </div>
        </div>
        <div class="clears"></div>

        <div class="add-in ">
            <div class="chose">
             <input type="checkbox" value="200">
            </div>
            <p>Общение с другими сервис – поставщиками или сервис - центрами от имени клиента </p>
            <div class="infa">
              <div class="descript">
                <div class="des-name">При возникновении технических вопросов с поставщиками, например интернета или телефонии:</div>
                <div class="des-des">наши инженеры смогут квалифицированно общаться от имени компании клиента</div>
              </div>
            </div>
        </div>
        <div class="clears"></div>

        <div class="add-in ">
            <div class="chose">
             <input type="checkbox" value="200">
            </div>
            <p>Доставка оборудования в сервис – центр и обратно (стоимость доставки оплачивается отдельно)  </p>
            <div class="infa">
              <div class="descript">
                <div class="des-name">Доставка неисправного оборудования клиента в сервис-центр и обратно силами наших специалистов:</div>
                <div class="des-des">Стоимость доставки не входит в стоимость договора обслуживания</div>
              </div>
            </div>
        </div>
        <div class="clears"></div>

        <div class="add-in ">
            <div class="chose">
             <input type="checkbox" value="200">
            </div>
            <p>Доставка оборудования в сервис – центр и обратно (стоимость доставки входит в стоимость)  </p>
            <div class="infa">
              <div class="descript">
                <div class="des-name">Доставка неисправного оборудования клиента в сервис-центр и обратно силами наших специалистов:</div>
                <div class="des-des">Стоимость доставки входит в стоимость договора обслуживания</div>
              </div>
            </div>
        </div>
        <div class="clears"></div>

        <div class="add-in ">
            <div class="chose">
             <input type="checkbox" value="200">
            </div>
            <p>Отслеживание состояния ремонта оборудования  </p>
            <div class="infa">
              <div class="descript">
                <div class="des-name">Контроль стадий выполнения ремонта неисправного оборудования клиента:</div>
                <div class="des-des">в сервис-центре производителя</div>
              </div>
            </div>
        </div>
        <div class="clears"></div>

        <div class="add-in ">
            <div class="chose">
             <input type="checkbox" value="200">
            </div>
            <p>Обучение пользователей работе с программным обеспечением  </p>
            <div class="infa">
              <div class="descript">
                <div class="des-name">Персональное или групповое обучение пользователей клиента:</div>
                <div class="des-des">нюансам использования программного обеспечения</div>
              </div>
            </div>
        </div>
        <div class="clears"></div>

        <div class="add-in ">
            <div class="chose">
             <input type="checkbox" value="200">
            </div>
            <p>Контроль за состоянием тонера в картриджах  </p>
            <div class="infa">
              <div class="descript">
                <div class="des-name">С помощью специализированного ПО, клиент получает отчеты:</div>
                <div class="des-des">о расходе тонера в картриджах и сигналы о скором окончании тонера</div>
              </div>
            </div>
        </div>
        <div class="clears"></div>

        <div class="add-in ">
            <div class="chose">
             <input type="checkbox" value="200">
            </div>
            <p>Продление лицензионного антивируса (входит в стоимость обслуживания)</p>
            <div class="infa">
              <div class="descript">
                <div class="des-name">При окончании срока действия лицензионного антивируса на компьютере клиента:</div>
                <div class="des-des">установка продления  антивируса производится за счет стоимости договора обслуживания</div>
              </div>
            </div>
        </div>
        <div class="clears"></div>
      </div><!--blogz-->
    </div><!--add-serv-->

    <div class="sum">
      <p>Стоимость <span><input id="count" type="text" value="0" size="4" style="text-align: center;"></span>руб/месяц</p>
      <input type="submit" class="sum-sub"  value="Распечатать">
    </div>
    <div class="clears"></div>

    <div class="sub">
      <table width="800" >
          <tr>
            <td class=left-tab>
              <input type="text" class="long" placeholder="Название организации">
              <input type="text" placeholder="Имя" >
              <input type="text" class="rig" placeholder="Телефон">
            </td>
            <td class=right-tab>
              <input type="submit" value="Отправить">
            </td>
          </tr>
        
        

      </table>
          

          <?php ActiveForm::end(); ?>

    </div>

    <?php echo Html::a('<i class="fa glyphicon glyphicon-hand-up"></i> Privacy Statement',
     ['/site/mpdf-demo-1'],
      [
        'class'=>'btn btn-danger',
        'target'=>'_blank',
        'data-toggle'=>'tooltip',
        'title'=>'Will open the generated PDF file in a new window'
      ]
    ); ?>
    <?php echo("<p>Привет</p>") ?>

  </div><!--select-blog-->

  <div class="clears"></div>
  <div class="anti_foot"></div>
</div><!--wrapper-->

<footer>
  <div class="fcontact clear-fix">
    <a href="callto:84951140450" class=ftel>8(495)11-404-50</a><br>
    <a href="mailto:support@xmemo.ru" class="fmail"><span>E-mail:</span> support@xmemo.ru</a>
    <p>xMemo © 2009-2014</p>
  </div>
    <a class="pushka" href="http://pushca.ru">Создание и поддержка сайтов<br>PUSH creative agency</a>
</footer>

<!-- S.M. -->

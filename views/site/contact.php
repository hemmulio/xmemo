<div class="wrapper minus" id="map">

        <header class="contact">
            <div class="container co">
              <a href='/site/index' class="logo"><img src="/images/logo.png" height="130" width="258"></a>
                <div class="menu">
                  <ul class="menu_list">
                    <li><a href='/site/index'>О компании</a></li>
                    <li><a href="#">Информация</a>
                          <ul>
                            <li><a href='/site/calc'>Цены и тарифы</a></li>
                            <li><a href='/site/about'>Порядок выполнения работ</a></li>
                            <li><a href='/site/about'>Гарантии</a></li>
                            <li><a href='/site/partner'>Партнерство</a></li>
                          </ul>
                     </li>
                        <li><a href='/site/ask'>ЗаДАТЬ ВОПРОС</a></li>
                        <li class="active"><a href='/site/contact'>Контакты</a></li>
                  </ul>
                </div>
            </div>

        </header>
    <div class="shad"></div>

<div class="container z">
    <div class="left-lop cont">
      <div class="content-lo">
        <h1>КОНТАКТЫ</h1>
        <p>г. Москва, Хлебозаводский пр., 7строение 9, офис 508<br><br>

        8(495)11-404-50<br><br>

        По всем возникающим вопросам пишите на
        <a href="#" class="contr">support@xmemo.ru</a></p><br>
        <div class="soc">
            <a href="#" class="skype"></a>
            <a href="#" class="odno"></a>
            <a href="#" class="vk"></a>
            <a href="#" class="fb"></a>
        </div><!--soc-->
      </div>
    </div>
  </div>

</div><!--wrapper-->

<div class="clear"></div>
<footer class="pos">
    <div class="fcontact clear-fix">
        <a href="callto:84951140450" class=ftel></a><br>
        <a href="mailto:support@xmemo.ru" class="fmail"></a>
        <p>xMemo © 2009-2014</p>
    </div>

</footer>

<script src="http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU" type="text/javascript"></script>
    <script type="text/javascript">
        // Как только будет загружен API и готов DOM, выполняем инициализацию
        ymaps.ready(init);
        function init () {
            // Создание экземпляра карты и его привязка к контейнеру с
            // заданным id ("map")
            var myMap = new ymaps.Map('map', {
                    // При инициализации карты, обязательно нужно указать
                    // ее центр и коэффициент масштабирования
                    center: [55.767078,37.604269], // Нижний Новгород
                    zoom: 16
                });

            // Создание метки
            var myPlacemark = new ymaps.Placemark(
            // Координаты метки
            [55.767078,37.604269]
            );

        // Добавление метки на карту
        myMap.geoObjects.add(myPlacemark);
       // $('#map .ymaps-map').css('z-index',-1).hide();
          }
    </script>

<!-- S.M. -->

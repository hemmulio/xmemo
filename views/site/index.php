

<div class="wrapper s">
        <header>
            <div class="cover">
                <div class="cover-bg">
                    <div class="cover-in"><div class="puska"></div>
                        <div class="container">
                            <a href='/site/index' class="logo"><img src="/images/logo.png" height="130" width="258"></a>
                            <div class="menu">
                                <ul class="menu_list">
                                    <li class="active"><a href="/site/index">О компании</a></li>
                                    <li><a href="#">Информация</a>
                                        <ul>
                                            <li><a href='/site/calc'>Цены и тарифы</a></li>
                                            <li><a href='/site/about'>Порядок выполнения работ</a></li>
                                            <li><a href='/site/about'>Гарантии</a></li>
                                            <li><a href='/site/partner'>Партнерство</a></li>
                                        </ul>
                                    </li>
                                    <li><a href='/site/ask'>ЗаДАТЬ ВОПРОС</a></li>
                                    <li><a href='/site/contact'>Контакты</a></li>
                                </ul></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clears"></div>
        </header>
  

        <div class="container">
           <div class="bodz">
             <h1>О КОМПАНИИ</h1>
               Компания XMEMO была основана в 2009 году. Профессионально и в срок мы оказываем полный спектр услуг ИТ-аутсорсинга, монтажа ЛВС, аудита, ремонта компьютерных систем. Обеспечиваем надежную защиту информации и целостности всей ИТ-структуры, на базе любых ОС: Windows, Linux, FreeBSD и прочих.<br><br>

                Основные услуги компании:<br>
                     •  Абонентское обслуживание компьютеров<br>
                     •  Аудит, техническая поддержка и безопасность ИТ-инфаструктуры<br>
                     •  Внедрение информационных систем<br>
                     •  Настройка АТС и ip телефонии<br>
                     •  Монтаж, настройка и обслуживание сетей<br>
                     •  Установка и настройка видеонаблюдения, домофонов и охранных систем<br>
                     •  Продажа компьютерного оборудования<br>
                     •  Продажа программного обеспечения<br>
                     •  Заправка и ремонт картриджей<br><br>

                Не нужно громких фраз, рекламных акций, стратегий и схем. Благодаря тому, что работу будут вести только высококвалифицированные специалисты, мы даем гарантию на достижение любой задачи, вне зависимости от уровня ее сложности. Секрет успеха прост: для того чтобы достичь вершин, важно сотрудничать исключительно с лучшими специалистами. Наш высокий уровень квалификации подтвердят десятки отзывов благодарных клиентов. Будьте в их числе.
             <div class="anti_foot"></div>
           </div><!--bodz-->

           <div class="download">
               <a class="dowlo" href="#">Скачать коммерческое предложение</a>
           </div>


        </div>

       <div class="clears"></div>

<footer>
  <div class="fcontact clear-fix">
    <a href="callto:84951140450" class=ftel>8(495)11-404-50</a><br>
    <a href="mailto:support@xmemo.ru" class="fmail"><span>E-mail:</span> support@xmemo.ru</a>
    <p>xMemo © 2009-2014</p>
  </div>
    <a class="pushka" href="http://pushca.ru">Создание и поддержка сайтов<br>PUSH creative agency</a>
</footer>

<!-- S.M. -->

$(function() {
    $('div.infa').each(function(){
        myTip = $(this);

        var tip = myTip.children('.descript'); //найдем блоки с подсказками

        myTip.hoverIntent(
            showTip,
            function(){tip.fadeOut(300);}
        );

        function showTip(e){
            xM = e.pageX,
            yM = e.pageY,
            tipW = tip.outerWidth(true),
            tipH = tip.outerHeight(true),
            winW = $(window).width(),
            winH = $(window).height(),
            scrollwinH = $(window).scrollTop(),
            scrollwinW = $(window).scrollLeft(),
            curwinH = $(window).scrollTop() + $(window).height();
            if ( winW - xM < scrollwinW  ) {
                tip.removeClass('descript').addClass('descript-left');
            } else {
                tip.removeClass('descript-left').addClass('descript');
            }
            if ( yM > scrollwinH + tipH && yM > curwinH / 2 ) {
                tip.addClass('a-top').removeClass('a-bot');;
            } else {
                tip.removeClass('a-top').addClass('a-bot');
            }
            tip.fadeIn(100).css('display','block');
            e.preventDefault();
        };

    });
});
